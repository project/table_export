
     
      Table Export
      Provided by www.vision-media.ca        
     
      ------------------------------------------------------------------------------- 
      INSTALLATION
      ------------------------------------------------------------------------------- 
      
      To install table export you will need to download the visibility_api module, 
      simply enable both modules and any table using the theme('table'); function 
      will be exportable.
      
      NOTE: This module currently requires a 'hook' into tables via the phptemplate_table
      override. If you wish to override the table theme please override theme_table_export_table 
      instead. 
      
      -------------------------------------------------------------------------------
      PERMISSIONS
      ------------------------------------------------------------------------------- 
      
      administer table export

          


